# Build several interesting Hello World programs

TARGETS=hello1_cpp hello2_cpp

all: $(TARGETS)

hello1_cpp: hello1.cpp
	g++ -g -Wall -o hello1            hello1.cpp

hello2_cpp: hello2.cpp
	g++ -g -Wall -o hello2          hello2.cpp

clean:
	rm -f $(TARGETS) *.o

