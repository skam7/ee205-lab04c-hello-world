///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World
///
/// This is an Object Oriented C++ Hello World program
///
/// @file hello1.cpp
/// @version 1.0
///
/// @author Shannon Kam <skam7@hawaii.edu>
/// @date   02_09_2021 
///////////////////////////////////////////////////////////////////////////////
using namespace std;

#include <iostream>
//#include <string>       

string helloWorld() {
   
   return "Hello World!";

}

int main(){
   
  cout << helloWorld() << endl;
  
  return 0;

}
